package main

import (
	"fmt"
	"math"
)

func findRepeatedElement(n int, array []int) (repeatedValue int) {
	l := len(array)
	for i := 0; i < l; i++ {
		absValue := math.Abs(float64(array[i]))
		if array[int(absValue)] > 0 {

			array[int(absValue)] = -array[int(absValue)]
		} else {
			repeatedValue = int(math.Abs(float64(array[i])))
			return repeatedValue
		}
	}
	return repeatedValue
}

func main() {
	var N int = 5
	array := []int{1, 2, 3, 4, 2, 4, 5}
	repeatedValue := findRepeatedElement(N, array)
	if repeatedValue == 0 {
		fmt.Println("no repeated value found")
	} else {
		fmt.Println("First repeated value is : ", repeatedValue)
	}
}
