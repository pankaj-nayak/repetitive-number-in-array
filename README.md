Problem: Find first repetitive number in an array. 
I/p : N=5 Arr[] = [1, 3, 2, 4, 2, 4, 5] where 1<= A[I] <=5 and Length of array should be >= N

Constraints:
 1. No sorting 
 2. No extra space 
 3. T(n) <= O(n)
